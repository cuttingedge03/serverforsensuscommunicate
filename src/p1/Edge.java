package p1;

/**
 * Represents an edge in a graph.
 * 
 * @author Stewart Wright
 * 
 */
public class Edge {
	public Node target;
	public double weight;

	public Edge(Node argTarget, double argWeight) {
		target = argTarget;
		weight = argWeight;
	}
}
