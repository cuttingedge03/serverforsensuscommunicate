package p1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {

	private static ServerSocket serverSocket;
	private static Socket clientSocket;
	private static InputStreamReader inputStreamReader;
	private static BufferedReader bufferedReader;
	private static String message;
	static List<String> fromClientList;

	public static void main(String[] args) {

		try {
			serverSocket = new ServerSocket(4444); // Server socket

		} catch (IOException e) {
			System.out.println("Could not listen on port: 4444\n");
		}

		System.out.println("Server started. Listening to the port 4444");
		fromClientList = new ArrayList<String>();
		while (true) {
			try {

				clientSocket = serverSocket.accept();// accept the client
														// connection

				// inputStreamReader = new InputStreamReader(
				// clientSocket.getInputStream());
				// bufferedReader = new BufferedReader(inputStreamReader); //
				// get
				// // the
				// // client
				// // message
				// message = bufferedReader.readLine();
				ObjectInputStream objectInput = new ObjectInputStream(
						clientSocket.getInputStream());

				fromClientList = (ArrayList<String>) objectInput.readObject();
				HexLightsOut hl = new HexLightsOut(fromClientList);
				List<String> paths = new ArrayList<String>();
				paths = hl.getList();
				ObjectOutputStream objectOutput = new ObjectOutputStream(
						clientSocket.getOutputStream());
				objectOutput.writeObject(paths);

				// System.out.println(message);
				// inputStreamReader.close();
				// clientSocket.close();

			} catch (Exception ex) {
				ex.printStackTrace();
				// clientSocket.close();
				// System.out.println("Problem in message reading");
			}
		}

	}
}