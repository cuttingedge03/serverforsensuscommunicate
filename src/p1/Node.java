package p1;

import java.util.List;


public class Node implements Comparable<Node> {
	public final String name;
	public List<Edge> adjacencies;
	public int centerY;
	public int centerX;
	public boolean isInRoute;
	public boolean isWalkable;
	public double minDistance = Double.POSITIVE_INFINITY;
	public Node previous;

	public Node(String argName) {
		name = argName;
	}

	public String toString() {
		return name;
	}

	public int compareTo(Node other) {
		return Double.compare(minDistance, other.minDistance);
	}
}
