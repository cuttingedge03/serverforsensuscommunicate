package p1;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * Servlet implementation class Serv1
 */
@WebServlet("/Serv1")
public class Serv1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Serv1() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
//		System.out.println("doget");
//		response.getOutputStream().println("Hurray !! This Servlet Works");
		// response.setContentType("text/html");

//		ObjectOutputStream out = new ObjectOutputStream(
//				response.getOutputStream());
		Enumeration<String> paramNames = request.getParameterNames();
		String params[] = new String[6];
		int i = 0;
		while (paramNames.hasMoreElements()) {
			String paramName = (String) paramNames.nextElement();

//			System.out.println(paramName);
			String[] paramValues = request.getParameterValues(paramName);
			params[i] = paramValues[0];

//			System.out.println(params[i]);
			i++;

		}
//		HexLightsOut hl = new HexLightsOut(params);
//		List<String> paths = new ArrayList<>();
//		paths = hl.getList();
//		String json = new Gson().toJson(paths);
//
//		response.setContentType("application/json");
//		response.setCharacterEncoding("UTF-8");
//		response.getWriter().write(json);
		// out.writeObject(json);
		// if (params[0].equals("nawin")) {
		//
		// if (params[1].equals("shiva")) {
		// out.writeObject("success");
		// } else {
		// out.writeObject("wrong password");
		// }
		// } else {
		// out.writeObject("wrong username");
		// }
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
//		System.out.println("dopost");
		doGet(request, response);

	}

}