package p1;

public class Hexagon {
	public double centerX;
	public double centerY;
	public double radius;
	public boolean isPassable;

	public Hexagon(double centerX, double centerY, double radius, boolean isPassable) {
		this.centerX = centerX;
		this.centerY = centerY;
		this.radius = radius;
		this.isPassable=isPassable;
	}

	public double getCenterX() {
		return centerX;
	}

	public double getCenterY() {
		return centerY;
	}


	public double getRadius() {
		return radius;
	}

}
