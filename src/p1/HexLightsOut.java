package p1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class HexLightsOut {

	private static final int NUM_HEX_CORNERS = 6;
	private static final int CELL_RADIUS = 4;
	public int width, height;
	private int[] mCornersX = new int[NUM_HEX_CORNERS];
	private int[] mCornersY = new int[NUM_HEX_CORNERS];
	Node v[][];
	private static HexGridCell mCellMetrics = new HexGridCell(CELL_RADIUS);
	public String sHeight, sWidth, sX, sY, dX, dY;
	List<String> list;

	public HexLightsOut(List<String> list) {
		sX = list.get(0);
		sY = list.get(1);
		dX = list.get(2);
		dY = list.get(3);
		sHeight = list.get(4);
		sWidth = list.get(5);
		initializeDijkstra();
	}

	public void initializeDijkstra() {
		width = (int) (Float.parseFloat(sWidth) / 4);
		height = (int) (Float.parseFloat(sHeight) / 4);
		v = new Node[height][width];
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				v[j][i] = new Node("(" + String.valueOf(j) + ","
						+ String.valueOf(i) + ")");
				v[j][i].adjacencies = new ArrayList<Edge>();

				if (i >= 110 && i <= 111 && j >= 9 && j <= 90) {
					v[j][i].isWalkable = false;
				} else if (i >= 110 && i <= 120 && j >= 90 && j <= 91) {
					v[j][i].isWalkable = false;
				} else if (i >= 120 && i <= 121 && j >= 90 && j <= 102) {
					v[j][i].isWalkable = false;
				} else if (i >= 108 && i <= 120 && j >= 102 && j <= 103) {
					v[j][i].isWalkable = false;
				} else if (i >= 108 && i <= 109 && j >= 102 && j <= 126) {
					v[j][i].isWalkable = false;
				} else if (i >= 104 && i <= 118 && j >= 127 && j <= 128) {
					v[j][i].isWalkable = false;
				} else if (i >= 118 && i <= 119 && j >= 127 && j <= 164) {
					v[j][i].isWalkable = false;
				} else if (i >= 110 && i <= 118 && j >= 164 && j <= 165) {
					v[j][i].isWalkable = false;
				} else if (i >= 102 && i <= 107 && j >= 159 && j <= 160) {
					v[j][i].isWalkable = false;
				} else if (i >= 107 && i <= 108 && j >= 153 && j <= 159) {
					v[j][i].isWalkable = false;
				} else if (i >= 107 && i <= 111 && j >= 152 && j <= 153) {
					v[j][i].isWalkable = false;
				} else if (i >= 103 && i <= 109 && j >= 139 && j <= 140) {
					v[j][i].isWalkable = false;
				} else if (i >= 103 && i <= 105 && j >= 127 && j <= 139) {
					v[j][i].isWalkable = false;
				} else if (i >= 101 && i <= 102 && j >= 152 && j <= 168) {
					v[j][i].isWalkable = false;
				} else if (i >= 49 && i <= 102 && j >= 167 && j <= 168) {
					v[j][i].isWalkable = false;
				} else if (i >= 46 && i <= 49 && j >= 160 && j <= 168) {
					v[j][i].isWalkable = false;
				} else if (i >= 49 && i <= 54 && j >= 148 && j <= 151) {
					v[j][i].isWalkable = false;
				} else if (i >= 55 && i <= 64 && j >= 149 && j <= 152) {
					v[j][i].isWalkable = false;
				} else if (i >= 89 && i <= 102 && j >= 152 && j <= 153) {
					v[j][i].isWalkable = false;
				} else if (i >= 16 && i <= 18 && j >= 91 && j <= 102) {
					v[j][i].isWalkable = false;
				} else if (i >= 24 && i <= 25 && j >= 86 && j <= 105) {
					v[j][i].isWalkable = false;
				} else if (i >= 14 && i <= 61 && j >= 16 && j <= 17) {
					v[j][i].isWalkable = false;
				} else if (i >= 61 && i <= 62 && j >= 11 && j <= 17) {
					v[j][i].isWalkable = false;
				} else if (i >= 62 && i <= 111 && j >= 12 && j <= 15) {
					v[j][i].isWalkable = false;
				} else if (i >= 101 && i <= 112 && j >= 15 && j <= 17) {
					v[j][i].isWalkable = false;
				} else if (i >= 73 && i <= 101 && j >= 17 && j <= 18) {
					v[j][i].isWalkable = false;
				} else if (i >= 100 && i <= 101 && j >= 19 && j <= 41) {
					v[j][i].isWalkable = false;
				} else if (i >= 71 && i <= 110 && j >= 41 && j <= 42) {
					v[j][i].isWalkable = false;
				} else if (i >= 100 && i <= 111 && j >= 29 && j <= 30) {
					v[j][i].isWalkable = false;
				} else if (i >= 70 && i <= 110 && j >= 49 && j <= 54) {
					v[j][i].isWalkable = false;
				} else if (i >= 88 && i <= 110 && j >= 53 && j <= 54) {
					v[j][i].isWalkable = false;
				} else if (i >= 88 && i <= 110 && j >= 53 && j <= 54) {
					v[j][i].isWalkable = false;
				} else if (i >= 0 && i <= 2 && j >= 123 && j <= 150) {
					v[j][i].isWalkable = false;
				} else if (i >= 98 && i <= 100 && j >= 54 && j <= 66) {
					v[j][i].isWalkable = false;
				} else if (i >= 71 && i <= 87 && j >= 54 && j <= 56) {
					v[j][i].isWalkable = false;
				} else if (i >= 71 && i <= 88 && j >= 60 && j <= 63) {
					v[j][i].isWalkable = false;
				} else if (i >= 57 && i <= 61 && j >= 66 && j <= 70) {
					v[j][i].isWalkable = false;
				} else if (i >= 60 && i <= 70 && j >= 74 && j <= 86) {
					v[j][i].isWalkable = false;
				} else if (i >= 104 && i <= 108 && j >= 75 && j <= 82) {
					v[j][i].isWalkable = false;
				} else if (i >= 90 && i <= 98 && j >= 92 && j <= 101) {
					v[j][i].isWalkable = false;
				} else if (i >= 109 && i <= 115 && j >= 94 && j <= 99) {
					v[j][i].isWalkable = false;
				} else if (i >= 103 && i <= 108 && j >= 110 && j <= 118) {
					v[j][i].isWalkable = false;
				} else if (i >= 32 && i <= 60 && j >= 139 && j <= 143) {
					v[j][i].isWalkable = false;
				} else if (i >= 58 && i <= 59 && j >= 134 && j <= 141) {
					v[j][i].isWalkable = false;
				} else if (i >= 54 && i <= 59 && j >= 133 && j <= 134) {
					v[j][i].isWalkable = false;
				} else if (i >= 53 && i <= 54 && j >= 125 && j <= 134) {
					v[j][i].isWalkable = false;
				} else if (i >= 63 && i <= 99 && j >= 137 && j <= 143) {
					v[j][i].isWalkable = false;
				} else if (i >= 98 && i <= 99 && j >= 125 && j <= 143) {
					v[j][i].isWalkable = false;
				} else if (i >= 96 && i <= 98 && j >= 110 && j <= 125) {
					v[j][i].isWalkable = false;
				} else if (i >= 91 && i <= 96 && j >= 109 && j <= 110) {
					v[j][i].isWalkable = false;
				} else if (i >= 90 && i <= 91 && j >= 109 && j <= 117) {
					v[j][i].isWalkable = false;
				} else if (i >= 66 && i <= 68 && j >= 110 && j <= 118) {
					v[j][i].isWalkable = false;
				} else if (i >= 59 && i <= 66 && j >= 107 && j <= 110) {
					v[j][i].isWalkable = false;
				} else if (i >= 58 && i <= 59 && j >= 107 && j <= 112) {
					v[j][i].isWalkable = false;
				} else if (i >= 54 && i <= 63 && j >= 124 && j <= 125) {
					v[j][i].isWalkable = false;
				} else if (i >= 62 && i <= 63 && j >= 124 && j <= 137) {
					v[j][i].isWalkable = false;
				} else {
					v[j][i].isWalkable = true;
				}

				if (isOnStraightLine(112, 165, 107, 159, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(49, 160, 36, 152, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(43, 156, 48, 150, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(36, 150, 30, 160, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(30, 160, 0, 140, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(0, 133, 17, 101, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(6, 145, 10, 140, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(10, 140, 14, 142, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(5, 122, 19, 131, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(19, 131, 18, 140, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(25, 105, 30, 114, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(16, 91, 0, 51, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(0, 51, 35, 31, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(35, 31, 52, 70, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(52, 70, 46, 75, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(47, 56, 32, 68, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(32, 68, 38, 79, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(38, 79, 32, 82, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(25, 85, 15, 91, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(16, 44, 7, 33, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(7, 33, 15, 27, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(15, 27, 14, 17, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(40, 45, 50, 40, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(54, 125, 44, 118, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(44, 118, 42, 121, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(31, 137, 35, 124, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(59, 112, 50, 122, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(50, 122, 54, 125, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(34, 124, 39, 119, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(22, 38, 33, 31, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(32, 31, 40, 45, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(40, 45, 44, 53, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(44, 53, 46, 55, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(47, 58, 53, 70, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(53, 70, 46, 74, j, i)) {
					v[j][i].isWalkable = false;
				}
				if (isOnStraightLine(13, 141, 19, 138, j, i)) {
					v[j][i].isWalkable = true;
				}

				// else {
				// v[j][i].isWalkable = true;
				// }
			}
		}
		executeDijkstra();
	}

	public void executeDijkstra() {
		list = new ArrayList<String>();
		// Edge edge = new Edge(v[0][0], 0);
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				mCellMetrics.setCellIndex(i, j);
				mCellMetrics.computeCorners(mCornersX, mCornersY);
				v[j][i].centerX = mCellMetrics.getCenterX();
				v[j][i].centerY = mCellMetrics.getCenterY();
				if (v[j][i].isWalkable) {
					for (int k = 0; k < 6; k++) {
						int nI = mCellMetrics.getNeighborI(k);
						int nJ = mCellMetrics.getNeighborJ(k);

						if (nJ >= 0 && nI >= 0 && nJ < height && nI < width) {// to
							// ensure
							// that
							// there
							// are
							// no
							// neighbors
							// with
							// negative
							// index
							// System.out.println("inside " + j + " " + i + " "
							// + nJ
							// + " " + nI);
							if (v[nJ][nI].isWalkable) {
								Edge edge = new Edge(v[0][0], 1);
								edge.target = v[nJ][nI];
								edge.weight = 1;
								v[j][i].adjacencies.add(edge);
								// v[j][i].adjacencies[k].target = v[nJ][nI];
								// v[j][i].adjacencies[k].weight = 1;

							}
						}
					}
				}
			}
		}
		calculatePath();
	}

	public void calculatePath() {
		try {
			mCellMetrics.setCellByPoint((int) Float.parseFloat((sX)),
					(int) Float.parseFloat(sY));
			int sourceI = mCellMetrics.getIndexI();
			int sourceJ = mCellMetrics.getIndexJ();

			mCellMetrics.setCellByPoint((int) Float.parseFloat((dX)),
					(int) Float.parseFloat(dY));
			int destinationI = mCellMetrics.getIndexI();
			int destinationJ = mCellMetrics.getIndexJ();
			System.out.println("i " + destinationI);
			System.out.println("j " + destinationJ);
			computePaths(v[sourceJ][sourceI]);

			List<Node> path = getShortestPathTo(v[destinationJ][destinationI]);
			System.out.println("Distance to " + v[destinationJ][destinationI]
					+ ": " + v[destinationJ][destinationI].minDistance);
			System.out.println("Path: " + path);
			drawPath(path, v[destinationJ][destinationI].minDistance);
		} catch (Exception ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(new JFrame(),
					"Hexagon is out of bounds! Select another.");
		}

	}

	private void drawPath(List<Node> path, double minDistance) {
		int size = path.size();
		// double[] pointX = new double[size];
		// double[] pointY = new double[size];
		// double[] RempointX = new double[6];
		// double[] RempointY = new double[6];
		// int j=0,k=0;
		// for (int i = 0; i < size; i++) {
		// if (i < 3) {
		// RempointX[i] = path.get(i).centerX;
		// RempointY[i] = path.get(i).centerY;
		// }else if(i==size-3){
		// RempointX[3] = path.get(i).centerX;
		// RempointY[3] = path.get(i).centerY;
		// }else if(i==size-2){
		// RempointX[4] = path.get(i).centerX;
		// RempointY[4] = path.get(i).centerY;
		// }else if(i==size-1){
		// RempointX[5] = path.get(i).centerX;
		// RempointY[5] = path.get(i).centerY;
		// } else {
		// pointX[k] = path.get(i).centerX;
		// pointY[k] = path.get(i).centerY;
		// ++k;
		// }
		// }
		// double[] coeffs = SGFilter.computeSGCoefficients(3, 3, 4);
		// SGFilter sgFilter = new SGFilter(3, 3);
		// double[] smoothX = sgFilter.smooth(pointX, new double[0],
		// new double[0], coeffs);
		// double[] smoothY = sgFilter.smooth(pointY, new double[0],
		// new double[0], coeffs);

		for (int i = 0; i < size; i++) {
			// if (i < 3) {
			// list.add(String.valueOf(RempointX[i]));
			// list.add(String.valueOf(RempointY[i]));
			//
			// }else if(i==size-3){
			// list.add(String.valueOf(RempointX[3]));
			// list.add(String.valueOf(RempointY[3]));
			// }else if(i==size-2){
			// list.add(String.valueOf(RempointX[4]));
			// list.add(String.valueOf(RempointY[4]));
			// }else if(i==size-1){
			// list.add(String.valueOf(RempointX[4]));
			// list.add(String.valueOf(RempointY[4]));
			// }else {
			// list.add(String.valueOf(smoothX[l]));
			// list.add(String.valueOf(smoothY[l]));
			// ++l;
			// }
			list.add(String.valueOf(path.get(i).centerX));
			list.add(String.valueOf(path.get(i).centerY));

		}
	}

	public List<String> getList() {
		return list;
	}

	public static void computePaths(Node source) {
		source.minDistance = 0.;
		PriorityQueue<Node> NodeQueue = new PriorityQueue<Node>();
		NodeQueue.add(source);

		while (!NodeQueue.isEmpty()) {
			Node u = NodeQueue.poll();

			// // Visit each edge exiting u
			// for (Iterator<Edge> iterator = u.adjacencies.iterator(); iterator
			// .hasNext();) {
			// Edge e = (Edge) iterator.next();
			// Node v = e.target;
			// double weight = e.weight;
			// double distanceThroughU = u.minDistance + weight;
			// if (distanceThroughU < v.minDistance) {
			// NodeQueue.remove(v);
			// v.minDistance = distanceThroughU;
			// v.previous = u;
			// NodeQueue.add(v);
			// }
			for (Edge e : u.adjacencies) {
				Node v = e.target;
				double weight = e.weight;
				double distanceThroughU = u.minDistance + weight;
				if (distanceThroughU < v.minDistance) {
					NodeQueue.remove(v);
					v.minDistance = distanceThroughU;
					v.previous = u;
					NodeQueue.add(v);
				}
			}
		}
	}

	public static List<Node> getShortestPathTo(Node target) {
		List<Node> path = new ArrayList<Node>();
		for (Node Node = target; Node != null; Node = Node.previous)
			path.add(Node);
		Collections.reverse(path);
		return path;
	}

	public boolean isOnStraightLine(int x1, int y1, int x2, int y2, int y, int x) {
		float m = (x2 - x1) / (y2 - y1);
		if (x1 > x2) {
			if (y1 > y2) {
				if (Math.abs((y - y1) - m * (x - x1)) < 10 && x >= x2
						&& x <= x1 && y >= y2 && y <= y1)
					return true;
				else
					return false;
			} else {
				if (Math.abs((y - y1) - m * (x - x1)) < 10 && x >= x2
						&& x <= x1 && y <= y2 && y >= y1)
					return true;
				else
					return false;
			}
		} else if (x1 < x2) {
			if (y1 > y2) {
				if (Math.abs((y - y1) - m * (x - x1)) < 10 && x <= x2
						&& x >= x1 && y >= y2 && y <= y1)
					return true;
				else
					return false;
			} else {
				if (Math.abs((y - y1) - m * (x - x1)) < 10 && x <= x2
						&& x >= x1 && y <= y2 && y >= y1)
					return true;
				else
					return false;
			}
		}
		return false;
	}
}